package PolyhedronBuilder;

import Unfolding.IUnfoldablePolyhedron;

import javax.vecmath.Matrix4d;

/**
 * Contains data to be drawn.
 */
class TestData {

	/**
	 * The canvas width.
	 */
	private int m_width = 640;

	/**
	 * The canvas height.
	 */
	private int m_height = 480;

	/**
	 * The DPI scale.
	 */
	private double m_dpiScale = 1;

	/**
	 * Number of divisions for drawing spheres and hemispheres.
	 */
	private int m_divs = 36;

	/**
	 * Number of line divisions in spheres and hemispheres.
	 */
	private int m_lineDivs = 12;

	/**
	 * The index of the selected polyhedron.
	 */
	private int m_selected = 0;

	/**
	 * Whether to draw the unfolding or the 3D model.
	 */
	private boolean m_drawUnfolding = false;

	/**
	 * Polyhedra to draw.
	 */
	private IUnfoldablePolyhedron[] m_polyhedra = {
		PolyhedronBuilder.getCube(true),
		PolyhedronBuilder.getPyramid(true),
		PolyhedronBuilder.getSphere(m_divs, m_lineDivs, true),
		PolyhedronBuilder.getHemisphere(m_divs, m_lineDivs, true),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getPyramid(false),
		PolyhedronBuilder.getSphere(m_divs, m_lineDivs, false),
		PolyhedronBuilder.getHemisphere(m_divs, m_lineDivs, false)
	};

	/**
	 * Bounding boxes for the polyhedra.
	 */
	private IUnfoldablePolyhedron[] m_boundingBoxes = {
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false),
		PolyhedronBuilder.getCube(false)
	};

	/**
	 * Gets the canvas width.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The width.
	 */
	int getWidth(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_width*m_dpiScale) : m_width;
	}

	/**
	 * Gets the canvas height.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The height.
	 */
	int getHeight(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_height*m_dpiScale) : m_height;
	}

	/**
	 * Sets the canvas width.
	 * @param width The width.
	 */
	void setWidth(int width) { m_width = width; }

	/**
	 * Sets the canvas height.
	 * @param height The height.
	 */
	void setHeight(int height) { m_height = height; }

	/**
	 * Sets the DPI scale.
	 * @param dpiScale The DPI scale.
	 */
	void setDpiScale(double dpiScale) { m_dpiScale = dpiScale; }

	/**
	 * Increments the index of the selected polygon by val.
	 */
	void incrementSelected(int val) {
		int n = m_polyhedra.length;
		m_selected = (m_selected + val + n) % n;
	}

	/**
	 * Toggles whether or not to draw the unfolding.
	 */
	void toggleDrawUnfolding() { m_drawUnfolding = !m_drawUnfolding; }

	/**
	 * Gets the selected polyhedron.
	 * @return The selected polyhedron.
	 */
	IUnfoldablePolyhedron getSelectedPolyhedron() {
		return m_polyhedra[m_selected];
	}

	/**
	 * Gets the bounding box of the selected polyhedron.
	 * @return The bounding box of the selected polyhedron.
	 */
	IUnfoldablePolyhedron getSelectedBoundingBox() {
		return m_boundingBoxes[m_selected];
	}

	/**
	 * Gets whether or not to draw the unfolding.
	 * @return Whether or not to draw the unfolding.
	 */
	boolean getDrawUnfolding() { return m_drawUnfolding; }

	/**
	 * Rotates the selected polygon.
	 * @param axis The rotation axis ('x', 'y', or 'z')
	 * @param angle The angle (in radians) by which to rotate.
	 */
	void rotateSelectedPolyhedron(char axis, double angle) {
		Matrix4d rotate = new Matrix4d();
		rotate.setIdentity();
		switch(axis) {
			case 'x': rotate.rotX(angle); break;
			case 'y': rotate.rotY(angle); break;
			case 'z': rotate.rotZ(angle); break;
		}
		m_polyhedra[m_selected].transform(rotate);
		m_boundingBoxes[m_selected].transform(rotate);
	}
}
