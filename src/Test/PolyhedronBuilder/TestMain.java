package PolyhedronBuilder;

import SimpleGL.SimpleCanvasGL;
import SimpleGL.SimpleDrawGL;
import SimpleGL.SimpleFrameGL;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class TestMain {

	public static void main(String[] args) {

		String title = "Test GL App";
		TestData data = new TestData();

		Map<Component, String> layoutComponentMap = new HashMap<>();

		SimpleCanvasGL canvas = createCanvas(data);
		layoutComponentMap.put(canvas, BorderLayout.CENTER);

		new SimpleFrameGL(title, new int[]{data.getWidth(true), data.getHeight(true)}, layoutComponentMap);
	}

	/**
	 * Creates a canvas with the given data and mouse/keyboard listeners.
	 * @param data The data.
	 * @return The canvas.
	 */
	private static SimpleCanvasGL createCanvas(TestData data) {

		SimpleDrawGL draw = new TestDraw(data);
		SimpleCanvasGL canvas = new SimpleCanvasGL(draw);

		TestCanvasMouseIO mouseIO = new TestCanvasMouseIO(canvas, data);
		canvas.addMouseListener(mouseIO);
		canvas.addMouseMotionListener(mouseIO);
		canvas.addMouseWheelListener(mouseIO);

		TestCanvasKeyboardIO keyboardIO = new TestCanvasKeyboardIO(canvas, data);
		canvas.addKeyListener(keyboardIO);

		return canvas;
	}
}