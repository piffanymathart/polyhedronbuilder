package PolyhedronBuilder;

import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon2d;
import Geometry.Polygons.IPolygon3d;
import Polyhedron.IColouredPolyhedron;
import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;
import com.jogamp.opengl.GL2;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;

import static SimpleGL.GLDrawHelper.*;

class TestDraw extends SimpleDrawGL {

	private TestData m_data;

	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set3DMode(
			gl2,
			m_data.getWidth(true),
			m_data.getHeight(true),
			Math.PI/180.0*35.0
		);
	}

	@Override
	public void draw(GL2 gl2) {

		if(m_data.getDrawUnfolding() && m_data.getSelectedPolyhedron().getNetTree()!=null) draw2dScene(gl2);
		else draw3dScene(gl2);
	}

	private void draw2dScene(GL2 gl2) {

		GLSetupHelper.clearCanvas(gl2, new float[] {0f, 0f, 0f, 1f});
		int w = m_data.getWidth(true);
		int h = m_data.getHeight(true);

		GLSetupHelper.set2DMode(gl2, w, h);

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(m_data.getSelectedPolyhedron());
		IPolygon2d[] unfolding =  polyhedron.getUnfolding(w,h);

		drawPolygons2d(gl2, unfolding);

		gl2.glFlush();
	}

	private void draw3dScene(GL2 gl2) {
		GLSetupHelper.clearCanvas(gl2, new float[] {0f, 0f, 0f, 1f});

		GLSetupHelper.set3DMode(gl2, m_data.getWidth(true), m_data.getHeight(true), 35.0/180.0*Math.PI);

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(m_data.getSelectedPolyhedron());
		IUnfoldablePolyhedron boundingBox = new UnfoldablePolyhedron(m_data.getSelectedBoundingBox());

		Matrix4d translate = getTranslateZMatrix(-3);
		polyhedron.transform(translate);
		boundingBox.transform(translate);

		drawPolyhedron3d(gl2, polyhedron);
		drawPolyhedron3dOutline(gl2, boundingBox);

		gl2.glFlush();
	}

	private Matrix4d getTranslateZMatrix(double z) {
		Matrix4d matrix = new Matrix4d();
		matrix.setIdentity();
		matrix.setTranslation(new Vector3d(0,0,z));
		return matrix;
	}

	private static void drawPolyhedron3dOutline(GL2 gl2, IColouredPolyhedron polyhedron) {
		float[] strokeColor = getColorGL(new Color(255,255,255,255));

		gl2.glColor4f(strokeColor[0], strokeColor[1], strokeColor[2], strokeColor[3]);
		for(ILine3d edge : polyhedron.getEdges()) {
			drawPolygon3d(gl2, new Point3d[] {edge.getP1(), edge.getP2()});
		}
	}

	private static void drawPolyhedron3d(GL2 gl2, IColouredPolyhedron polyhedron) {
		float[] strokeColor = getColorGL(new Color(255,255,255,255));
		float[] fillColor = getColorGL(new Color(255,255,255,80));

		gl2.glColor4f(fillColor[0], fillColor[1], fillColor[2], fillColor[3]);
		for(IPolygon3d face : polyhedron.getFaces()) {
			fillPolygon3d(gl2, face.getVertices());
		}

		gl2.glColor4f(strokeColor[0], strokeColor[1], strokeColor[2], strokeColor[3]);
		for(ILine3d edge : polyhedron.getEdges()) {
			drawPolygon3d(gl2, new Point3d[] {edge.getP1(), edge.getP2()});
		}
	}

	private static void drawPolygons2d(GL2 gl2, IPolygon2d[] polygons) {
		float[] strokeColor = getColorGL(new Color(255,255,255,255));
		float[] fillColor = getColorGL(new Color(255,255,255,80));

		gl2.glColor4f(fillColor[0], fillColor[1], fillColor[2], fillColor[3]);
		for(IPolygon2d polygon : polygons) {
			fillPolygon2d(gl2, polygon.getVertices());
		}

		gl2.glColor4f(strokeColor[0], strokeColor[1], strokeColor[2], strokeColor[3]);
		for(IPolygon2d polygon : polygons) {
			int n = polygon.size();
			for (int i = 0; i < n; i++) {
				int j = (i == n - 1) ? 0 : i + 1;
				drawPolygon2d(gl2, new Point2d[]{polygon.get(i), polygon.get(j)});
			}
		}
	}

	private static float[] getColorGL(Color color) {
		if(color==null) return null;
		return new float[] {
			(float)color.getRed()/255.0f,
			(float)color.getGreen()/255.0f,
			(float)color.getBlue()/255.0f,
			(float)color.getAlpha()/255.0f
		};
	}
}
