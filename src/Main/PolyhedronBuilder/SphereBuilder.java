package PolyhedronBuilder;

import Polyhedron.PolyhedronHelper;
import Unfolding.INode;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.Node;
import Unfolding.UnfoldablePolyhedron;

import javax.vecmath.Point3d;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * A helper class for constructing polyhedral spheres.
 */
class SphereBuilder {

	/**
	 * The number of subdivisions in the spherical approximation.
	 */
	private int m_divs;

	/**
	 * The number of division lines.
	 */
	private int m_lineDivs;

	/**
	 * A row-column index to vertex index map.
	 */
	private Map<List<Integer>, Integer> m_vertexIndexMap;

	/**
	 * A row-column index to face index map.
	 */
	private Map<List<Integer>, Integer> m_faceIndexMap;

	/**
	 * The vertices.
	 */
	private Point3d[] v_vertices;

	/**
	 * The edge indices.
	 */
	private int[][] v_edgeIndices;

	/**
	 * The face indices.
	 */
	private int[][] v_faceIndices;

	/**
	 * Constructs a unit polyhedral sphere from number of subdivisions.
	 * @param divs The number of subdivisions.
	 */
	IUnfoldablePolyhedron create(int divs, int lineDivs, boolean unfoldable) {

		if(divs < 2) throw new IllegalArgumentException("divs must be greater than 2.");

		m_divs = divs;
		m_lineDivs = lineDivs;

		// compute vertices and face indices
		computeVertices();
		computeFaceIndices();

		// compute edge indices and clean up vertices
		generateEdgeIndicesFromFaceIndices();
		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(
			v_vertices,
			v_edgeIndices,
			v_faceIndices,
			new Color[v_faceIndices.length],
			null
		);

		new PolyhedronHelper().removeVertexDuplicates(polyhedron);

		// set fields
		polyhedron = new UnfoldablePolyhedron(
			v_vertices,
			v_edgeIndices,
			v_faceIndices,
			new Color[v_faceIndices.length],
			null
		);

		if(unfoldable) createUnfolding(polyhedron);
		return polyhedron;
	}

	/**
	 * Computes the vertices.
	 */
	private void computeVertices() {

		List<Point3d> vertices = new ArrayList<>();
		m_vertexIndexMap = new HashMap<>();

		int vertexIndex = 0;
		for(int row=0; row<=m_divs; row++) {
			for(int col=0; col<=m_divs; col++) {

				if(row==0 && col>0) continue;
				if(row==m_divs && col>0) continue;

				vertices.add(getVertex(row, col));
				m_vertexIndexMap.put(
					Arrays.asList(row, col),
					vertexIndex
				);
				vertexIndex++;
			}
		}

		v_vertices = vertices.toArray(new Point3d[vertices.size()]);
	}

	/**
	 * Generates edge indices based on specified line division parameters.
	 */
	private void generateEdgeIndicesFromFaceIndices() {

		List<int[]> edgeIndices = new ArrayList<>();

		int skip = (int) Math.round((double)m_divs/(double)m_lineDivs);

		// horizontal lines
		for(int row=0; row<=m_divs; row+=skip) {
			for(int col=0; col<=m_divs; col++) {
				if(row==0) continue;
				if(row==m_divs) continue;
				int edgeIndex1 = getVertexIndex(row, col);
				int edgeIndex2 = getVertexIndex(row, (col==m_divs) ? 0 : (col+1));
				edgeIndices.add(new int[] {edgeIndex1, edgeIndex2});
			}
		}

		// vertical lines
		for(int col=0; col<=m_divs; col+=skip) {
			for(int row=0; row<m_divs; row++) {
				int edgeIndex1 = getVertexIndex(row, (row==0 || row==m_divs) ? 0 : col);
				int edgeIndex2 = getVertexIndex(row+1, (row+1==0 || row+1==m_divs) ? 0 : col);
				edgeIndices.add(new int[] {edgeIndex1, edgeIndex2});
			}
		}

		v_edgeIndices = edgeIndices.toArray(new int[edgeIndices.size()][]);
	}

	/**
	 * Computes the face indices.
	 */
	private void computeFaceIndices() {

		List<int[]> faceIndices = new ArrayList<>();
		m_faceIndexMap = new HashMap<>();

		int faceIndex = 0;
		for(int row=0; row<m_divs; row++) {
			for(int col=0; col<m_divs; col++) {

				// add upper faces
				if(row > 0) {
					faceIndices.add(getFaceVertexIndices(row,col,true));
					m_faceIndexMap.put(getKey(row,col,true), faceIndex);
					faceIndex++;
				}

				// add lower faces
				if(row < m_divs-1) {
					faceIndices.add(getFaceVertexIndices(row,col,false));
					m_faceIndexMap.put(getKey(row,col,false), faceIndex);
					faceIndex++;
				}
			}
		}

		v_faceIndices = faceIndices.toArray(new int[faceIndices.size()][]);
	}

	/**
	 * Gets the specified vertex.
	 * @param row The row index.
	 * @param col The column index.
	 * @return The vertex.
	 */
	private Point3d getVertex(int row, int col) {

		double phi = Math.PI * (double)row / (double)m_divs;
		double theta = 2.0 * Math.PI * (double)col / (double)m_divs;

		return new Point3d(
			0.5*Math.cos(theta)*Math.sin(phi),
			0.5*Math.sin(theta)*Math.sin(phi),
			0.5*Math.cos(phi)
		);
	}

	/**
	 * Gets the key value associated with the specified row index, column
	 * index, and upper/lower triangle status.
	 * @param row The row index.
	 * @param col The column index.
	 * @param isUpper Whether the face is an upper or a lower triangle.
	 * @return The associated key value.
	 */
	private List<Integer> getKey(int row, int col, boolean isUpper) {
		return Arrays.asList(row, col, isUpper ? 0 : 1);
	}

	/**
	 * Gets the vertex indices associated with a face.
	 * @param row The row index of the face.
	 * @param col The column index of the face.
	 * @param isUpper Whether the face is an upper or a lower triangle.
	 * @return The vertex indices of the face.
	 */
	private int[] getFaceVertexIndices(int row, int col, boolean isUpper) {

		int nextCol = (col+1)%m_divs;

		if(isUpper) {
			int v1 = getVertexIndex(row, col);
			int v2 = getVertexIndex(row+1,(row == m_divs-1) ? 0 : col);
			int v3 = getVertexIndex(row, nextCol);
			return new int[] {v1, v2, v3};
		}
		else {
			int v1 = getVertexIndex(row+1, nextCol);
			int v2 = getVertexIndex(row, (row == 0) ? 0 : nextCol);
			int v3 = getVertexIndex(row+1, col);
			return new int[] {v1, v2, v3};
		}
	}

	/**
	 * Gets the index of the specified vertex.
	 * @param row The row index.
	 * @param col The column index.
	 * @return The vertex index.
	 */
	private int getVertexIndex(int row, int col) {

		List<Integer> key = Arrays.asList(row, col);
		return m_vertexIndexMap.get(key);
	}

	/**
	 * Creates a tree node specifying the unfolding.
	 */
	private void createUnfolding(IUnfoldablePolyhedron polyhedron) {

		// create grids of nodes so that we can refer to each node by row and column
		INode[][] upper = getUpperNodeGrid();
		INode[][] lower = getLowerNodeGrid();

		// for the first row, string the lower faces together
		for(int j=0; j<m_divs-1; j++) {
			lower[0][j].addChild(lower[0][j+1]);
		}

		// for the intermediate rows, string the faces together in alternating upper-lower pattern
		for(int i=1; i<m_divs-1; i++) {
			for(int j=0; j<m_divs; j++) {
				upper[i][j].addChild(lower[i][j]);
				if(j < m_divs-1) lower[i][j].addChild(upper[i][j+1]);
			}
		}

		// for the last row, string the upper faces together
		for(int j=0; j<m_divs-1; j++) {
			upper[m_divs-1][j].addChild(upper[m_divs-1][j+1]);
		}

		// for the first column, string the faces together in alternating lower-upper pattern
		for(int i=0; i<m_divs-1; i++) {
			lower[i][0].addChild(upper[i+1][0]);
		}

		polyhedron.setNetTree(lower[0][0]);
	}

	/**
	 * Creates a grid of upper triangular face nodes.
	 * @return The grid of upper triangular face nodes.
	 */
	private INode[][] getUpperNodeGrid() {

		INode[][] upper = new INode[m_divs][m_divs];
		for(int i=0; i<m_divs; i++) {
			for(int j=0; j<m_divs; j++) {
				Integer faceIndex = getFaceIndex(i, j, true);
				upper[i][j] = faceIndex==null ? null : new Node(faceIndex);
			}
		}
		return upper;
	}

	/**
	 * Creates a grid of lower triangular face nodes.
	 * @return The grid of lower triangular face nodes.
	 */
	private INode[][] getLowerNodeGrid() {

		INode[][] lower = new INode[m_divs][m_divs];
		for(int i=0; i<m_divs; i++) {
			for(int j=0; j<m_divs; j++) {
				Integer faceIndex = getFaceIndex(i, j, false);
				lower[i][j] = faceIndex==null ? null : new Node(faceIndex);
			}
		}
		return lower;
	}

	/**
	 * Gets the index of the specified face.
	 * @param row The row index.
	 * @param col The column index.
	 * @param isUpper Whether the face is a upper or lower triangle.
	 * @return The index of the specified face.
	 */
	private Integer getFaceIndex(int row, int col, boolean isUpper) {

		List<Integer> key = Arrays.asList(row, col, isUpper ? 0 : 1);
		return m_faceIndexMap.get(key);
	}
}
