package PolyhedronBuilder;

import Unfolding.IUnfoldablePolyhedron;

public interface PolyhedronBuilder {

	static IUnfoldablePolyhedron getCube(boolean unfoldable) {
		return new CubeBuilder().create(unfoldable);
	}

	static IUnfoldablePolyhedron getPyramid(boolean unfoldable) {
		return new PyramidBuilder().create(unfoldable);
	}

	static IUnfoldablePolyhedron getHemisphere(int divs, int lineDivs, boolean unfoldable) {
		return new HemisphereBuilder().create(divs, lineDivs, unfoldable);
	}

	static IUnfoldablePolyhedron getSphere(int divs, int lineDivs, boolean unfoldable) {
		return new SphereBuilder().create(divs, lineDivs, unfoldable);
	}
}
