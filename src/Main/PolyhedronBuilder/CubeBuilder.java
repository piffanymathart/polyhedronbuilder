package PolyhedronBuilder;

import Polyhedron.PolyhedronHelper;
import Unfolding.INode;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.Node;
import Unfolding.UnfoldablePolyhedron;

import javax.vecmath.Point3d;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * A helper class for constructing polyhedral cubes.
 */
class CubeBuilder {

	/**
	 * A vertex name to vertex index map.
	 */
	private Map<String, Integer> m_vertexIndexMap;

	/**
	 * A face name to face indices map.
	 */
	private Map<String, Integer> m_faceIndexMap;

	/**
	 * The vertices.
	 */
	private Point3d[] v_vertices;

	/**
	 * The face indices.
	 */
	private int[][] v_faceIndices;

	/**
	 * Constructs a polyhedral cube.
	 */
	IUnfoldablePolyhedron create(boolean unfoldable) {

		// compute vertices and face indices
		computeVertices();
		computeFaceIndices();
		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(
			v_vertices,
			new int[0][0],
			v_faceIndices,
			new Color[v_faceIndices.length],
			null
		);

		// compute edge indices and clean up vertices
		new PolyhedronHelper().generateEdgeIndicesFromFaceIndices(polyhedron);
		new PolyhedronHelper().removeVertexDuplicates(polyhedron);

		// set fields
		polyhedron = new UnfoldablePolyhedron(
			v_vertices,
			polyhedron.getEdgeIndices(),
			v_faceIndices,
			new Color[v_faceIndices.length],
			null
		);

		if(unfoldable) createUnfolding(polyhedron);
		return polyhedron;
	}

	/**
	 * Creates a tree node specifying the unfolding.
	 */
	private void createUnfolding(IUnfoldablePolyhedron polyhedron) {

		INode rootTop = new Node(m_faceIndexMap.get("top"));
		INode bottom = new Node(m_faceIndexMap.get("bottom"));
		INode left = new Node(m_faceIndexMap.get("left"));
		INode right = new Node(m_faceIndexMap.get("right"));
		INode near = new Node(m_faceIndexMap.get("near"));
		INode far = new Node(m_faceIndexMap.get("far"));

		rootTop.addChildren(new INode[] {left, right, near, far});
		right.addChild(bottom);

		polyhedron.setNetTree(rootTop);
	}
	/**
	 * Computes the cube's vertices.
	 */
	private void computeVertices() {

		v_vertices = new Point3d[8];
		m_vertexIndexMap = new HashMap<>();

		// construct vertices
		for(double x : new double[] {-0.5,0.5}) {
			for(double y : new double[] {-0.5,0.5}) {
				for(double z : new double[] {-0.5,0.5}) {
					String name = (y < 0 ? "bottom" : "top")
						+ (x < 0 ? "Left" : "Right")
						+ (z < 0 ? "Far" : "Near");
					Point3d p = new Point3d(x, y, z);
					addNewVertex(name, p);
				}
			}
		}
	}

	/**
	 * Computes the cube's face indices.
	 */
	private void computeFaceIndices() {

		v_faceIndices = new int[6][];
		m_faceIndexMap = new HashMap<>();

		addNewFaceIndices(
			"top",
			new String[] {"topLeftNear", "topRightNear", "topRightFar", "topLeftFar"}
		);

		addNewFaceIndices(
			"bottom",
			new String[] {"bottomLeftFar", "bottomRightFar", "bottomRightNear", "bottomLeftNear"}
		);

		addNewFaceIndices(
			"left",
			new String[] {"topLeftNear", "topLeftFar", "bottomLeftFar", "bottomLeftNear"}
		);

		addNewFaceIndices(
			"right",
			new String[] {"bottomRightNear", "bottomRightFar", "topRightFar", "topRightNear"}
		);

		addNewFaceIndices(
			"near",
			new String[] {"bottomLeftNear", "bottomRightNear", "topRightNear", "topLeftNear"}
		);

		addNewFaceIndices(
			"far",
			new String[] {"topLeftFar", "topRightFar", "bottomRightFar", "bottomLeftFar"}
		);
	}

	/**
	 * Adds a new vertex.
	 * @param name The vertex's name.
	 * @param p The vertex's position.
	 */
	private void addNewVertex(String name, Point3d p) {
		int index = m_vertexIndexMap.size();
		m_vertexIndexMap.put(name, index);
		v_vertices[index] = p;
	}

	/**
	 * Adds a new set of face indices.
	 * @param name The face's name.
	 * @param vertNames The names of the vertices in the face.
	 */
	private void addNewFaceIndices(String name, String[] vertNames) {
		int index = m_faceIndexMap.size();

		int[] faceInds = new int[vertNames.length];
		for(int i=0; i<vertNames.length; i++) {
			faceInds[i] = m_vertexIndexMap.get(vertNames[i]);
		}

		m_faceIndexMap.put(name, index);
		v_faceIndices[index] = faceInds;
	}
}
